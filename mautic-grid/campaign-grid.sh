#!/bin/bash

clear

echo "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓"
echo "┃   MAUTIC GRID CAMPAIGN     ┃"
echo "┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫"
echo "┃ Iniciando Instalação!      ┃"
echo "┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫"
echo "┃ Instalação em andamento... ┃"
echo "┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫"

wget https://bitbucket.org/moskoweb/cdn-files/raw/248ac46d8d52e4a926062cfe45187ff7b0b5af4f/mautic-grid/campaign.css --quiet

mv campaign.css app/bundles/CampaignBundle/Assets/css/

wget https://bitbucket.org/moskoweb/cdn-files/raw/248ac46d8d52e4a926062cfe45187ff7b0b5af4f/mautic-grid/campaign.js --quiet

mv campaign.js app/bundles/CampaignBundle/Assets/js/

wget https://bitbucket.org/moskoweb/cdn-files/raw/90e895ca734f9aec7398309df8d9d391e88f3776/mautic-grid/preview.html.php --quiet

mv preview.html.php app/bundles/CampaignBundle/Views/Event/

php app/console cache:clear --quiet && chmod -R g+rw * && php app/console mautic:assets:generate --quiet && php app/console cache:warmup --quiet

echo "┃ Instalação Concluída!      ┃"
echo "┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫"
echo "┃ Criado por Alan Mosko      ┃"
echo "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛"