document.addEventListener("DOMContentLoaded", function() {
	var params = window.location.search.substr(1).split("&");
	if (params) {
		Array.prototype.forEach.call( params, function(param, i){
			var a = param.split("=");
			
			// Replace [tag] for Value
			document.body.innerHTML = document.body.innerHTML.replace( "[v=" + a[0] + "]", decodeURIComponent(a[1]) );
			document.body.innerHTML = document.body.innerHTML.replace( "[u=" + a[0] + "]", a[0] + "=" + a[1] );
		});
	}
	document.body.innerHTML = document.body.innerHTML.replace( /\[v=[a-zA-Z]{1,}\]/gm, "" );
	document.body.innerHTML = document.body.innerHTML.replace( /\[u=[a-zA-Z]{1,}\]/gm, "" );
	
	if (params) {
		Array.prototype.forEach.call( params, function(param, i){
			var a = param.split("=");

			// Populate "Form" with Value
			Array.prototype.forEach.call(document.querySelectorAll( 'input[name*="' + a[0] + '"]' ), function(el, i){
				if ( el.value == '' ) {
					el.value = a[1];
				}
			});
		});
	}
});

function getParameter(name) {
	var params = window.location.search.substr(1).split("&");

	for (var i = 0; i < params.length; i++) {
		var p = params[i].split("=");
		if (p[0] == name) {
			return decodeURIComponent(p[1]);
		}
	}
	return '';
}