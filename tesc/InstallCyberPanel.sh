# !/bin/bash

clear

echo "═════════════════════════════════════════════════════"
echo "          CyberPanel Install by Alan Mosko           "
echo "═════════════════════════════════════════════════════"

echo "Domain Host:"
read hostname

echo "Admin Email:"
read admin_email

echo "$hostname" > /etc/hostname

hostname $hostname

echo "═════════════════════════════════════════════════════"
echo "               Start Install CyberPane               "
echo "═════════════════════════════════════════════════════"

yum update -y

yum install -y nano ncdu wget sudo zip unzip curl htop screen

yum -y upgrade

timedatectl set-timezone America/Sao_Paulo

sh <(curl https://cyberpanel.net/install.sh || wget -O - https://cyberpanel.net/install.sh)

cyberpanel createWebsite --package Default --owner admin --domainName $hostname --email $admin_email --php 7.0

cyberpanel issueSSL --domainName $hostname

cyberpanel hostNameSSL --domainName $hostname

rm -rf /home/$hostname/public_html/*

echo "<?php header('Location: https://$hostname:8090'); die(); ?>" > /home/$hostname/public_html/index.php
